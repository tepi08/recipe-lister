package jp.co.tepi.recipelister.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.Calendar;

import jp.co.tepi.recipelister.R;
import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.db.table.ScheduleTableManager;
import jp.co.tepi.recipelister.utility.Utility;
/**
 * Created by TSUGE on 2014/07/22.
 */
public class ScheduleAdapter extends CursorAdapter {

    private long thisWeekSundayTime;
    private long thisWeekSaturdayTime;

    public ScheduleAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);

        Calendar thisWeekSunday = Utility.getSundayThisWeek();
        thisWeekSundayTime = thisWeekSunday.getTimeInMillis();
        Calendar thisWeekSaturday = (Calendar) thisWeekSunday.clone();
        thisWeekSaturday.set(Calendar.DATE, thisWeekSaturday.get(Calendar.DATE) + 7);
        thisWeekSaturdayTime = thisWeekSaturday.getTimeInMillis();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.fragment_schedule_list_adapter, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        long time = cursor.getLong(cursor.getColumnIndex(ScheduleTableManager.TIME));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        TextView tv = (TextView) view.findViewById(R.id.scheduleAdapterDate);
        tv.setText(Utility.getDateStringFromTimeInMillis(cursor.getLong(cursor.getColumnIndex(ScheduleTableManager.TIME))));

        tv = (TextView) view.findViewById(R.id.scheduleAdapterRecipeName);
        String recipeName = cursor.getString(cursor.getColumnIndex(RecipeTableManager.RECIPE_NAME));
        if (recipeName == null || recipeName.equals("")) {
            tv.setVisibility(View.GONE);
        } else {
            tv.setVisibility(View.VISIBLE);
            tv.setText("" + recipeName);
        }

        //色変更
        if (time < thisWeekSundayTime) {
            view.setBackgroundColor(context.getResources().getColor(R.color.gray));
        } else {
            view.setBackgroundResource(R.drawable.bg_list);
        }
    }
}
