package jp.co.tepi.recipelister.db.table;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import org.json.JSONException;
import org.json.JSONObject;

import jp.co.tepi.recipelister.db.DatabaseOpenHelper;
/**
 * Created by TSUGE on 2014/07/18.
 */
public class RecipeTableManager extends TableManager {

    private static RecipeTableManager tableManager;
    public static final String TABLE_NAME = "recipe";
    public static final Uri CONTENT_URI =  Uri.parse("content://" + DatabaseOpenHelper.AUTHORITY + "/" + TABLE_NAME);

    public static final String RECIPE_NAME = "recipeName";

    public static RecipeTableManager newIntance(){
        if(tableManager == null){
            tableManager = new RecipeTableManager();
        }

        return tableManager;
    }

    @Override
    public void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + BaseColumns._ID 			+ " INTEGER PRIMARY KEY,"
                + RECIPE_NAME				+ " TEXT"
                + ");");
    }

    @Override
    public void updateOrInsert(JSONObject json, ContentResolver cr) throws JSONException {
    }

    public void updateOrInsert(String id, String name, ContentResolver cr) throws JSONException {
        ContentValues values = new ContentValues();
        values.put(RECIPE_NAME, name);

        int count = 0;
        if(id != null){
            count = cr.update(CONTENT_URI, values, BaseColumns._ID + " = '" + id + "'", null);
        }

        if(count == 0){
            cr.insert(CONTENT_URI, values);
        }
    }

    public void delete(String id, ContentResolver cr){
        cr.delete(CONTENT_URI, BaseColumns._ID + " = '" + id + "'", null);
    }
}
