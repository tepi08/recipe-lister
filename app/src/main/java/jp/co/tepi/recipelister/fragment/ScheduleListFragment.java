package jp.co.tepi.recipelister.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.TextViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import jp.co.tepi.recipelister.R;
import jp.co.tepi.recipelister.SelectMenuActivity;
import jp.co.tepi.recipelister.adapter.ScheduleAdapter;
import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.db.table.ScheduleTableManager;
import jp.co.tepi.recipelister.utility.Constants;
import jp.co.tepi.recipelister.utility.PreferenceUtility;
import jp.co.tepi.recipelister.utility.UIUtility;
import jp.co.tepi.recipelister.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final static int CODE_REQUEST_MENU = 100;
    private int loaderId;
    private ScheduleAdapter adapter;
    private Calendar fromDate;
    private Calendar toDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        addDatesToList();

        View v = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        adapter = new ScheduleAdapter(getActivity(), null, false);
        ListView lv = (ListView) v.findViewById(R.id.listView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = (Cursor) adapter.getItem(position);

                Intent intent = new Intent(getActivity(), SelectMenuActivity.class);
                intent.putExtra(Constants.KEY_MENU_DATE, c.getLong(c.getColumnIndex(ScheduleTableManager.TIME)));
                startActivityForResult(intent, CODE_REQUEST_MENU);
            }
        });
        return v;
    }

    /**
     * DBに日付を必要なら追加
     */
    private void addDatesToList() {
        //まず当日の週の日曜日を求める
        Calendar sundayThisWeek = Utility.getSundayThisWeek();

        //最後に追加した日付を取得する
        long lastAddedDate = PreferenceUtility.getLastAddedDate(getActivity());
        //最後に追加したのが当日の週の日曜日でなければ日付を追加する必要がある
        if (lastAddedDate != sundayThisWeek.getTimeInMillis()) {
            Calendar twoWeeksLater = (Calendar) sundayThisWeek.clone();
            twoWeeksLater.set(Calendar.DATE, sundayThisWeek.get(Calendar.DATE) + 14);
            if (lastAddedDate == -1) {
                //始めての時は最初の日付を当日の週の週前の日曜日にする
                Calendar twoWeeksBefore = (Calendar) sundayThisWeek.clone();
                twoWeeksBefore.set(Calendar.DATE, twoWeeksBefore.get(Calendar.DATE) - 14);
                lastAddedDate = twoWeeksBefore.getTimeInMillis();
            }
            long differenceTime = twoWeeksLater.getTimeInMillis() - lastAddedDate;
            int differenceDays = (int) (differenceTime / 1000 / 60 / 60 / 24);

            //2週間後から追加する
            //(もし時間が空いてしまった場合にその前の日付も追加するが見たいのは当日の週からの2週間なので先に追加する)
            for (int i = differenceDays; i >= 0; i--) {
                ScheduleTableManager.newIntance().updateOrInsert(-1, twoWeeksLater.getTimeInMillis(), getActivity().getContentResolver());
                twoWeeksLater.set(Calendar.DATE, twoWeeksLater.get(Calendar.DATE) - 1);
            }

            PreferenceUtility.setLastAddedDate(getActivity(), sundayThisWeek.getTimeInMillis());
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        Loader<Cursor> loader = getLoaderManager().initLoader(0, null, this);
        loaderId = loader.getId();
    }

    @Override
    public void onStop() {
        super.onStop();
        getLoaderManager().destroyLoader(loaderId);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Calendar showStartDate = Utility.getSundayThisWeek();

        Calendar twoWeeksLater = (Calendar) showStartDate.clone();

        showStartDate.set(Calendar.DATE, showStartDate.get(Calendar.DATE) - 14);
        twoWeeksLater.set(Calendar.DATE, twoWeeksLater.get(Calendar.DATE) + 14);
        final String where = ScheduleTableManager.TIME + " >= " + showStartDate.getTimeInMillis() + " AND " + ScheduleTableManager.TIME + " <= " + twoWeeksLater.getTimeInMillis();
        return new CursorLoader(getActivity(), ScheduleTableManager.CONTENT_URI, null, where, null, ScheduleTableManager.TIME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        adapter.notifyDataSetChanged();

        //常に2週前分を表示するので、初期位置を14日分ずらす
        ListView lv = (ListView) getView().findViewById(R.id.listView);
        lv.setSelection(14);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.schedule, menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_REQUEST_MENU && resultCode == Activity.RESULT_OK) {
            int id = data.getIntExtra(Constants.KEY_SELECT_MENU_RESULT_ID, -1);
            long date = data.getLongExtra(Constants.KEY_MENU_DATE, -1);
            Log.d("TSUGE", "onActivityResult " + id + " " + Utility.getDateStringFromTimeInMillis(date));

            ScheduleTableManager.newIntance().updateOrInsert(id, date, getActivity().getContentResolver());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                showSelectDateDialog(R.string.title_share_from,
                new OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fromDate = Calendar.getInstance();
                        fromDate.set(year , monthOfYear, dayOfMonth);
                        Utility.resetTime(fromDate);

                        showSelectDateDialog(R.string.title_share_to,
                                new OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        toDate = Calendar.getInstance();
                                        toDate.set(year , monthOfYear, dayOfMonth);
                                        Utility.resetTime(toDate);

                                        share();
                                    }
                                });
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSelectDateDialog(int stringResId, OnDateSetListener listener){
        final Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        int padding = getResources().getDimensionPixelSize(R.dimen.datepicker_title_padding);
        TextView tv = new TextView(getActivity());
        TextViewCompat.setTextAppearance(tv, R.style.DialogTitleTextAppearance);
        tv.setText(stringResId);
        tv.setPadding(padding, padding, padding, padding);
        tv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorDialogTitle));
        tv.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));

        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                                                                       listener,
                                                                       year, month, day);
        datePickerDialog.setCustomTitle(tv);
        datePickerDialog.show();
        UIUtility.setDatePickerColor(getActivity(), datePickerDialog);
    }

    private void share() {
        if(toDate == null || fromDate == null){
            return;
        }
        if(toDate.getTimeInMillis() < fromDate.getTimeInMillis()){
            Toast.makeText(getActivity(), R.string.error_share_date_not_after, Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuilder shareString = new StringBuilder();
        Cursor schedules = ScheduleTableManager.getSchedules(getActivity().getContentResolver(), fromDate, toDate);
        while (schedules.moveToNext()){
            String date = Utility.getDateStringFromTimeInMillis(schedules.getLong(schedules.getColumnIndex(ScheduleTableManager.TIME)));
            String recipeName = schedules.getString(schedules.getColumnIndex(RecipeTableManager.RECIPE_NAME));

            shareString.append(date)
                    .append("\n");

            if(recipeName != null){
                shareString.append(recipeName);
                shareString.append("\n");
            }
            shareString.append("\n");
        }
        schedules.close();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareString.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
