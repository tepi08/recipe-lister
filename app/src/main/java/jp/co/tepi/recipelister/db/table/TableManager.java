package jp.co.tepi.recipelister.db.table;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public abstract class TableManager {
	public abstract void createTable(SQLiteDatabase db);
	
	public abstract void updateOrInsert(JSONObject json, ContentResolver cr) throws JSONException;
}
