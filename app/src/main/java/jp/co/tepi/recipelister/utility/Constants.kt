package jp.co.tepi.recipelister.utility

class Constants {
    companion object {
        const val KEY_MENU_DATE : String  = "menuDate"
        const val KEY_SELECT_MENU_RESULT_ID : String  = "menuId"
    }
}