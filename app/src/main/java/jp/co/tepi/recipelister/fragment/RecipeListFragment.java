package jp.co.tepi.recipelister.fragment;


import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONException;

import jp.co.tepi.recipelister.R;
import jp.co.tepi.recipelister.adapter.RecipeAdapter;
import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.db.table.ScheduleTableManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private int loaderId;
    private RecipeAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        adapter = new RecipeAdapter(getActivity(), null, false);
        ListView lv = (ListView) v.findViewById(R.id.listView);
        lv.setAdapter(adapter);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog d = new AlertDialog.Builder(getActivity()).setTitle("メニューを削除")
                                                                      .setMessage("メニューを削除しますか？")
                                                                      .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                          @Override
                                                                          public void onClick(DialogInterface dialog, int which) {
                                                                              Cursor c = (Cursor) adapter.getItem(position);
                                                                              String id = c.getString(c.getColumnIndex(BaseColumns._ID));
                                                                              RecipeTableManager.newIntance().delete(id, getActivity().getContentResolver());

                                                                              ScheduleTableManager.newIntance().deleteScheduleWithRecipe(id, getActivity().getContentResolver());
                                                                              dialog.dismiss();
                                                                          }
                                                                      })
                                                                      .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                          @Override
                                                                          public void onClick(DialogInterface dialog, int which) {
                                                                              dialog.dismiss();
                                                                          }
                                                                      })
                                                                      .create();
                d.show();
                return true;
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        Loader<Cursor> loader = getLoaderManager().initLoader(0, null, this);
        loaderId = loader.getId();
    }

    @Override
    public void onStop() {
        super.onStop();
        getLoaderManager().destroyLoader(loaderId);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), RecipeTableManager.CONTENT_URI, null, "", null, RecipeTableManager.RECIPE_NAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.recipe, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add) {
            final EditText et = new EditText(getActivity());

            AlertDialog d = new AlertDialog.Builder(getActivity()).setTitle("メニューを追加")
                                                                  .setMessage("メニューを追加してください")
                                                                  .setView(et)
                                                                  .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                      @Override
                                                                      public void onClick(DialogInterface dialog, int which) {
                                                                          try {
                                                                              RecipeTableManager.newIntance()
                                                                                                .updateOrInsert("", et.getText()
                                                                                                                      .toString(), getActivity().getContentResolver());
                                                                          } catch (JSONException e) {
                                                                              e.printStackTrace();
                                                                          }

                                                                          dialog.dismiss();
                                                                      }
                                                                  })
                                                                  .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                      @Override
                                                                      public void onClick(DialogInterface dialog, int which) {
                                                                          dialog.dismiss();
                                                                      }
                                                                  })
                                                                  .create();
            d.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            d.show();
            et.requestFocus();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
