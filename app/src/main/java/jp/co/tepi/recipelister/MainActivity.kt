package jp.co.tepi.recipelister

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import jp.co.tepi.recipelister.adapter.MainPagerAdapter

class MainActivity : AppCompatActivity() {
    private var adapter: MainPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = MainPagerAdapter(this, supportFragmentManager)

        val vp : ViewPager = findViewById(R.id.viewPager)
        vp.adapter = adapter
    }

}
