package jp.co.tepi.recipelister.db.table;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import jp.co.tepi.recipelister.db.DatabaseOpenHelper;
/**
 * Created by TSUGE on 2014/07/25.
 */
public class ScheduleTableManager extends TableManager {
    private static ScheduleTableManager tableManager;
    public static final String TABLE_NAME = "schedule";
    public static final Uri CONTENT_URI = Uri.parse("content://" + DatabaseOpenHelper.AUTHORITY + "/" + TABLE_NAME);

    public static final String RECIPE_ID = "recipeId";
    public static final String TIME = "time";

    public static ScheduleTableManager newIntance() {
        if (tableManager == null) {
            tableManager = new ScheduleTableManager();
        }

        return tableManager;
    }

    @Override
    public void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY,"
                + RECIPE_ID + " INTEGER,"
                + TIME + " INTEGER"
                + ");");
    }

    @Override
    public void updateOrInsert(JSONObject json, ContentResolver cr) throws JSONException {

    }

    public void updateOrInsert(int recipeID, long time, ContentResolver cr){
        ContentValues values = new ContentValues();
        values.put(TIME, time);
        if(recipeID != -1){
            values.put(RECIPE_ID, recipeID);
        }

        int count = 0;
        count = cr.update(CONTENT_URI, values, TIME + " = '" + time + "'", null);

        if(count == 0){
            cr.insert(CONTENT_URI, values);
        }
    }

    public void deleteScheduleWithRecipe(String recipeId, ContentResolver cr){
        cr.delete(CONTENT_URI, RECIPE_ID + " = '" + recipeId + "'", null);
    }

    public static Cursor getSchedules(ContentResolver cr, Calendar fromDate, Calendar toDate) {
        final String where = TIME + " >= " + fromDate.getTimeInMillis() +
                " AND " + TIME + " <= " + toDate.getTimeInMillis();
        return cr.query(CONTENT_URI, null, where, null, TIME + " ASC");
    }
}
