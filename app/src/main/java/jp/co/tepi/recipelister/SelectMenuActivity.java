package jp.co.tepi.recipelister;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import jp.co.tepi.recipelister.adapter.RecipeAdapter;
import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.utility.Constants;

public class SelectMenuActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private RecipeAdapter adapter;
    private int loaderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_menu);

        adapter = new RecipeAdapter(this, null, false);
        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = (Cursor) adapter.getItem(position);

                Intent intent = new Intent();
                intent.putExtra(Constants.KEY_SELECT_MENU_RESULT_ID, c.getInt(c.getColumnIndex(BaseColumns._ID)));
                intent.putExtra(Constants.KEY_MENU_DATE, getIntent().getExtras().getLong(Constants.KEY_MENU_DATE));

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();

        Loader<Cursor> loader = getLoaderManager().initLoader(0, null, this);
        loaderId = loader.getId();
    }

    @Override
    public void onStop() {
        super.onStop();
        getLoaderManager().destroyLoader(loaderId);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, RecipeTableManager.CONTENT_URI, null, null, null, RecipeTableManager.RECIPE_NAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

}
