package jp.co.tepi.recipelister.utility

import android.content.Context
import android.content.SharedPreferences

class PreferenceUtility {
    companion object {
        private const val KEY_LAST_ADDED_DATE = "lastAddedDate"

        @JvmStatic
        private fun getPreference(context: Context): SharedPreferences {
            return context.getSharedPreferences("app", Context.MODE_PRIVATE)
        }

        @JvmStatic
        fun getLastAddedDate(context: Context): Long {
            return getPreference(context).getLong(KEY_LAST_ADDED_DATE, -1)
        }

        @JvmStatic
        fun setLastAddedDate(context: Context, value: Long) {
            getPreference(context).edit().putLong(KEY_LAST_ADDED_DATE, value).commit();
        }
    }
}