package jp.co.tepi.recipelister.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import jp.co.tepi.recipelister.R;
import jp.co.tepi.recipelister.db.table.RecipeTableManager;
/**
 * Created by TSUGE on 2014/07/22.
 */
public class RecipeAdapter extends CursorAdapter {
    public RecipeAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.fragment_recipe_list_adapter, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tv = (TextView) view.findViewById(R.id.recipeAdapterRecipeName);
        tv.setText(cursor.getString(cursor.getColumnIndex(RecipeTableManager.RECIPE_NAME)));

    }
}
