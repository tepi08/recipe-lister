package jp.co.tepi.recipelister.utility

import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.DatePicker
import android.widget.NumberPicker
import jp.co.tepi.recipelister.R

class UIUtility {
    companion object {
        @JvmStatic
        fun setDatePickerColor(context: Context, dialog: Dialog) {
            val res = context.resources;

            //まずはダイアログのタイトルの部分のDividerの色変更
            val titleDivider = getViewByIdName(res, dialog, "titleDivider");
            titleDivider?.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))

            //DatePickerのDividerの色を変える
            val datePicker: DatePicker = getViewByIdName(res, dialog, "datePicker") as DatePicker;
            val numberPickerIdNameList = arrayOf("year", "month", "day");
            numberPickerIdNameList.forEach {
                val numberPicker: NumberPicker? = getViewByIdName(res, datePicker, it) as? NumberPicker;
                if (numberPicker != null){
                    setNumberPickerDividerColor(context, numberPicker)
                }
            }
        }

        @JvmStatic
        private fun setNumberPickerDividerColor(context: Context, numberPicker: NumberPicker?) {
            val numberPickerFields = NumberPicker::class.java.declaredFields;

            for (field in numberPickerFields) {
                if (field.name.equals("mSelectionDivider")) {
                    field.isAccessible = true

                    val drawable = GradientDrawable()
                    drawable.setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    field.set(numberPicker, drawable)

                    break;
                }
            }
        }

        @JvmStatic
        private fun getViewByIdName(res: Resources, parentView: View, idName: String): View? {
            val viewId = res.getIdentifier(idName, "id", "android")
            return parentView.findViewById(viewId)
        }

        @JvmStatic
        private fun getViewByIdName(res: Resources, dialog: Dialog, idName: String): View? {
            val viewId = res.getIdentifier(idName, "id", "android")
            return dialog.findViewById(viewId)
        }
    }
}