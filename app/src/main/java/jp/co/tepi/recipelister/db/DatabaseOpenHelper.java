package jp.co.tepi.recipelister.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.db.table.ScheduleTableManager;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
	public static final String AUTHORITY = "jp.co.tepi.recipelister.db";

	public DatabaseOpenHelper(Context context) {
		super(context, "app.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
        RecipeTableManager.newIntance().createTable(db);
        ScheduleTableManager.newIntance().createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
