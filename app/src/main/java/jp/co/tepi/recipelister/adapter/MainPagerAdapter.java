package jp.co.tepi.recipelister.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import jp.co.tepi.recipelister.R;
import jp.co.tepi.recipelister.fragment.RecipeListFragment;
import jp.co.tepi.recipelister.fragment.ScheduleListFragment;
/**
 * Created by TSUGE on 2014/07/25.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();
    private String[] pageTitles;

    public MainPagerAdapter(Context c, FragmentManager fm) {
        super(fm);

        fragmentList.add(new RecipeListFragment());
        fragmentList.add(new ScheduleListFragment());

        pageTitles = new String[]{c.getString(R.string.fragment_title_recipe), c.getString(R.string.fragment_title_schedule)};
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }
}
