package jp.co.tepi.recipelister.db;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jp.co.tepi.recipelister.db.table.RecipeTableManager;
import jp.co.tepi.recipelister.db.table.ScheduleTableManager;

public class DatabaseContentProvider extends ContentProvider {
	private DatabaseOpenHelper openHelper;

    private static final UriMatcher matcher;
    private static final int MATCH_RECIPE_TABLE = 1;
    private static final int MATCH_SCHEDULE_TABLE = 2;

    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(DatabaseOpenHelper.AUTHORITY, RecipeTableManager.TABLE_NAME, MATCH_RECIPE_TABLE);
        matcher.addURI(DatabaseOpenHelper.AUTHORITY, ScheduleTableManager.TABLE_NAME, MATCH_SCHEDULE_TABLE);
    }

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public boolean onCreate() {
		openHelper = new DatabaseOpenHelper(getContext());
		return false;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
//		Log.d("TSUGE", "▼▼▼▼▼▼▼▼ insert ▼▼▼▼▼▼▼▼");
//		Log.d("TSUGE", "uri = " + uri.toString());
//		Log.d("TSUGE", "values = " + values.toString());

		SQLiteDatabase db = openHelper.getWritableDatabase();

		long rowId = db.insert(getTableNameFromUri(uri), null, values);
        // If the insert succeeded, the row ID exists.
		Uri noteUri = null;
        if (rowId > 0) {
    		noteUri = ContentUris.withAppendedId(Uri.parse(uri.toString() +"/"), rowId);
            getContext().getContentResolver().notifyChange(noteUri, null);
        }

//		Log.d("TSUGE", "▲▲▲▲▲▲▲▲ insert ▲▲▲▲▲▲▲▲");
        return noteUri;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
//		Log.d("TSUGE", "▼▼▼▼▼▼▼▼ delete ▼▼▼▼▼▼▼▼");
//		Log.d("TSUGE", "uri = " + uri.toString());
//		Log.d("TSUGE", "selection = " + selection);
//		Log.d("TSUGE", "selectionArgs = " + selectionArgs);

		SQLiteDatabase db = openHelper.getWritableDatabase();
		int count = db.delete(getTableNameFromUri(uri), selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
//		getContext().getContentResolver().notifyChange(uri, null);
//		Log.d("TSUGE", "deleted " + count + " rows");
//		Log.d("TSUGE", "▲▲▲▲▲▲▲▲ delete ▲▲▲▲▲▲▲▲");
		return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
//		Log.d("TSUGE", "▼▼▼▼▼▼▼▼ update ▼▼▼▼▼▼▼▼");
//		Log.d("TSUGE", "uri = " + uri.toString());
//		Log.d("TSUGE", "values = " + values.toString());
//		Log.d("TSUGE", "selection = " + selection);
//		Log.d("TSUGE", "selectionArgs = " + selectionArgs);
		
		SQLiteDatabase db = openHelper.getWritableDatabase();
		int count = db.update(getTableNameFromUri(uri), values, selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
//		getContext().getContentResolver().notifyChange(uri, null);
		
//		Log.d("TSUGE", "▲▲▲▲▲▲▲▲ update ▲▲▲▲▲▲▲▲");
		return count;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
//		Log.d("TSUGE", "▼▼▼▼▼▼▼▼ query ▼▼▼▼▼▼▼▼");
//		Log.d("TSUGE", "uri = " + uri.toString());
//		Log.d("TSUGE", "values = " + projection);
//		Log.d("TSUGE", "selection = " + selection);
//		Log.d("TSUGE", "selectionArgs = " + selectionArgs);
//		Log.d("TSUGE", "sortOrder = " + sortOrder);
		
        SQLiteDatabase db = openHelper.getReadableDatabase();
		Cursor c = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String tableName = getTableNameFromUri(uri);
        if(tableName.equals(ScheduleTableManager.TABLE_NAME)){
            //ScheduleTableの時はRecipeの名前を取得できるようにとジョインする
            tableName = tableName + " LEFT OUTER JOIN " + RecipeTableManager.TABLE_NAME + " ON (" + RecipeTableManager.TABLE_NAME + "."+ BaseColumns._ID + " = " + tableName + "." + ScheduleTableManager.RECIPE_ID + ")";
        }

        qb.setTables(tableName);

        c = qb.query(db, projection, selection, selectionArgs, null /* no group */, null /* no filter */, sortOrder);


        c.setNotificationUri(getContext().getContentResolver(), uri);
//		Log.d("TSUGE", "▲▲▲▲▲▲▲▲ query ▲▲▲▲▲▲▲▲");
        return c;
	}

    private String getTableNameFromUri(Uri uri){
        String tableName = "";
        switch (matcher.match(uri)){
            case MATCH_RECIPE_TABLE:
                tableName = RecipeTableManager.TABLE_NAME;
                break;
            case MATCH_SCHEDULE_TABLE:
                tableName = ScheduleTableManager.TABLE_NAME;
                break;
        }
        return tableName;
    }
	
	public static void copyDatabase(String from, String to, String backupPath){
		// Local database
		try {
			InputStream input = new FileInputStream(from);

		    // create directory for backup
		    File dir = new File(backupPath);
		    dir.mkdir();

		    // Path to the external backup
		    OutputStream output = new FileOutputStream(to);

		    // transfer bytes from the Input File to the Output File
		    byte[] buffer = new byte[1024];
		    int length;
		    while ((length = input.read(buffer))>0) {
		        output.write(buffer, 0, length);
		    }

		    output.flush();
		    output.close();
		    input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
