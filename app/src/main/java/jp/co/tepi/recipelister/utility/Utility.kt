package jp.co.tepi.recipelister.utility

import java.text.SimpleDateFormat
import java.util.*

class Utility {
    companion object {
        val sdf = SimpleDateFormat("yyyy/MM/dd (E)")

        @JvmStatic
        fun getDateStringFromTimeInMillis(timeInMillis: Long): String {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timeInMillis
            return sdf.format(calendar.time)
        }

        @JvmStatic
        fun resetTime(calendar: Calendar) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        }

        @JvmStatic
        fun getSundayThisWeek(): Calendar {
            val sundayThisWeek = Calendar.getInstance()
            while (sundayThisWeek.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                sundayThisWeek.set(Calendar.DATE, sundayThisWeek.get(Calendar.DATE) - 1)
            }
            Utility.resetTime(sundayThisWeek)
            return sundayThisWeek
        }
    }
}